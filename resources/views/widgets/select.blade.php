@php

$name = $widget['name'] ?? 'unamed';
$label = $widget['label'] ?? $name;
$all_label = $widget['all_label'] ?? $name;
$value = $widget['value'] ?? '';

$options = $widget['options'] ?? [];


@endphp

@includeWhen(!empty($widget['wrapper']), 'backpack::widgets.inc.wrapper_start')

<div x-data="{
    name: {{json_encode($name)}},
    current:'',
    url:new URL(window.location),
    data: {{json_encode($options)}}
}" x-init="current = url.searchParams.get(name);">
<label>{!! $label !!}</label>

<select x-model="current" x-on:change="url.searchParams.set(name,current); window.location.href=url.toString()" name="{!! $name !!}" style="width: 100%" class="form-control select2_field">
    <option value="">{!! $all_label !!}</option>
    <template x-for="(item,id) in data">
        <option 
        :selected="current == id"
        :value="id"
        x-text="item" ></option>
    </template>
</select>
</div>
@includeWhen(!empty($widget['wrapper']), 'backpack::widgets.inc.wrapper_end')