<?php

namespace KDA\Backpack\Widget;

use KDA\Laravel\PackageServiceProvider;

class WidgetServiceProvider extends PackageServiceProvider
{
   use \KDA\Laravel\Traits\HasMigration;
   use \KDA\Laravel\Traits\HasConfig;
   use \KDA\Laravel\Traits\HasViews;
   use \KDA\Laravel\Traits\HasHelper;

   protected $configs = [
       'kda/backpack/widgets.php'=> 'kda.backpack.widgets'
   ];


   protected $registerViews = 'kda-backpack-widgets';
  
   protected $publishViewsTo = 'vendor/backpack/base';

   protected function packageBaseDir () {
       return dirname(__DIR__,1);
   }


}
