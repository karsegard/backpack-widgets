<?php
use Backpack\CRUD\app\Library\Widget;

if (!function_exists('kda_no_warranty_alert')) {
    /*
     * Returns the name of the guard defined
     * by the application config
     */
    function kda_no_warranty_alert()
    {
        return  Widget::add([
            'type'         => 'alert',
            'class'        => 'alert alert-error mb-2',
            'heading'      => 'Important !',
            'content'      => 'Modifier ces paramêtres peut empêcher le site de fonctionner. <br/> Les modifications faites ici sont exclues de toute garantie. <br/> Si vous avez des questions: <a href="mailto:contact@karsegard.ch">envoyez nous un mail</a>',
            'close_button' => false, // show close button or not
        ]);
    }
}



if (!function_exists('kda_help')) {
    /*
     * Returns the name of the guard defined
     * by the application config
     */
    function kda_help($help,$title="Aide",$style="primary")
    {
        return  Widget::add([
            'type'         => 'alert',
            'class'        => 'alert alert-'.$style.' mb-2',
            'heading'      => $title,
            'content'      => $help,
            'close_button' => false, // show close button or not
        ]);
    }
}
